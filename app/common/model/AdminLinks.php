<?php

namespace app\common\model;

use think\Model;

class AdminLinks extends Model
{
    /**
     * 获取更新日志列表
     * @param $where
     * @param $limit
     * @return array
     */
    public function getLinksList($where, $limit)
    {
        try {

            $list = $this->where($where)->order('link_id','desc')->paginate($limit);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0,'success', $list);
    }
}