<?php

declare (strict_types = 1);

namespace app\admin\controller\admin;

use app\admin\controller\Base;
use app\common\model\AdminArticle;
use app\common\model\AdminCate;
use app\common\model\AdminUpdateLog;

class Article extends Base
{
    protected $middleware = ['AdminCheck','AdminPermission'];

    public function index()
    {
        if (request()->isAjax()) {

            $limit = input('param.limit');
            $title = input('param.title');

            $where = [];
            if (!empty($title)) {
                $where[] = ['title', 'like', '%' . $title . '%'];
            }

            $articleModel = new AdminArticle();
            $list = $articleModel->getArticleList($where, $limit);

            return json(pageReturn($list));
        }

        return $this->fetch();
    }

    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $has = AdminArticle::where('title', $param['title'])->find();
            if (!empty($has)) {
                return jsonReturn(-1, '该文章标题已经存在');
            }

            $param['create_time'] = date('Y-m-d H:i:s');
            $param['cate_id'] = $param['select'];
            unset($param['select']);

            AdminArticle::insert($param);

            return jsonReturn(0, '新增成功');
        }

        $cateList = AdminCate::field('cate_id as value,name,parent_id')->where('status', 1)->select()->toArray();
        $cateList = makeTree($cateList);

        return $this->fetch('', [
            'cate' => json_encode($cateList)
        ]);
    }

    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');
            $param['cate_id'] = $param['select'];
            unset($param['select']);

            $has = AdminArticle::where('title', $param['title'])->where('article_id', '<>', $param['article_id'])->find();
            if (!empty($has)) {
                return jsonReturn(-1, '该文章标题已经存在');
            }

            $param['update_time'] = date('Y-m-d H:i:s');
            AdminArticle::where('article_id', $param['article_id'])->update($param);

            return jsonReturn(0, '更新成功');
        }

        $id = input('param.id');

        $info = AdminArticle::where('article_id', $id)->find();
        $cateList = AdminCate::field('cate_id as value,name,parent_id')->where('status', 1)->select()->toArray();
        $cateList = makeTree($cateList);

        $res = AdminCate::where('cate_id', $info['cate_id'])->find();
        $default = [
            'name' => $res['name'],
            'value' => $res['cate_id']
        ];

        return $this->fetch('', [
            'info' => $info,
            'cate' => json_encode($cateList),
            'default' => json_encode($default)
        ]);
    }

    public function del()
    {
        $id = input('param.id');

        AdminArticle::where('article_id', $id)->update([
            'is_delete' => 2
        ]);

        return jsonReturn(0, '删除成功');
    }
}