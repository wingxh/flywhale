<?php
/**
 * Created by PhpStorm.
 * Date: 2021/4/6
 * Time: 4:33 PM
 */
namespace app\admin\controller\admin;

class Upload
{
    // 执行文件上传
    public function doUpload()
    {
        $file = request()->file('edit');

        // 上传到本地服务器
        try {

            // 存到本地
            $saveName = \think\facade\Filesystem::disk('public')->putFile('', $file);

        } catch (\think\exception\ValidateException $e) {
            return jsonReturn(-2, $e->getMessage());
        }
        return json(['code' => 0, 'data' => '/upload/' . $saveName, 'msg' => '上传成功']);
    }
}