<?php
declare (strict_types = 1);

namespace app\command;

use app\common\service\Socket;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class SocketServe extends Command
{
    protected function configure()
    {
        $this->setName('sockeio')
            ->addArgument('action', Argument::OPTIONAL, "start|stop|restart|reload|status|connections", 'start')
            ->addOption('host', 'H', Option::VALUE_OPTIONAL, 'the host of workerman service.', null)
            ->addOption('port', 'p', Option::VALUE_OPTIONAL, 'the port of workerman service.', null)
            ->addOption('daemon', 'd', Option::VALUE_NONE, 'Run the workerman service in daemon mode.')
            ->setDescription('web sender Server for suwork');
    }

    protected function execute(Input $input, Output $output)
    {
        $action = $input->getArgument('action');

        if (DIRECTORY_SEPARATOR !== '\\') {
            if (!in_array($action, ['start', 'stop', 'reload', 'restart', 'status', 'connections'])) {
                $output->writeln("<error>Invalid argument action:{$action}, Expected start|stop|restart|reload|status|connections .</error>");
                return false;
            }

            global $argv;
            array_shift($argv);
            array_shift($argv);
            array_shift($argv);
            array_unshift($argv, 'think', $action);
        }

        $logo =<<<EOL
                                              
                              \`-,                             
                              |   `\                           
                              |     \                          
                           __/.- - -.\,__                      
                      _.-'`              `'"'--..,__           
                  .-'`                              `'--.,_    
               .'`   _                         _ ___       `)  
             .'   .'` `'-.                    (_`  _`)  _.-'   
           .'    '--.     '.                 .-.`"`@ .-'""-,   
  .------~'     ,.---'      '-._      _.'   /   `'--'"""".-'   
/`        '   /`  _,..-----.,__ `''''`/    ;__,..--''--'`      
`'--.,__ '    |-'`             `'---'|     |                   
        `\    \                       \   /                    
         |     |                       '-'                     
          \    |                                               
           `\  |                                               
             \/  
EOL;

        $output->writeln($logo . PHP_EOL);

        $ApiServerShow = <<<EOL
API SERVER LISTEN: http://127.0.0.1:23520
EOL;
        $output->writeln($ApiServerShow . PHP_EOL);

        // 运行socket.io服务
        Socket::start();
    }
}
